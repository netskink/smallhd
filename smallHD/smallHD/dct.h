#pragma once

// Book form
int32_t dct0(const double *pBufferIn, const int32_t length, double *pResult);
// matlab / octave form
int32_t dct(const double *pBufferIn, const int32_t length, double *pResult);
int32_t dump_double_buffer(const double *pBuf, const int32_t len);

