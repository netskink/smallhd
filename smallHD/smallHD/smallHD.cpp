// smallHD.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdint.h>
#include "smallHD.h"
#include "pgm_snippet.h"
#include "math.h"
#include "dct.h"


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	

extern unsigned char *frame_buffer;
extern unsigned int frame_width;
extern unsigned int frame_height;

const char pFileName1[] = { "kitten.pgm" };
//const char pFileName1[] = { "kitten_tiny.pgm" };  // so I dont have a large input during iterative testing. 412x366
//const char pFileName1[] = { "kitten_small.pgm" };  // so I dont have a large input during iterative testing. 412x366
const char pFileName3[] = { "output.pgm" };  // so I dont have a large input during iterative testing. 412x366

// Used to test neighborhood assignments
double pOffsetTesty[] = { 0.0, 1.0, 2.0, 3.0,
				          4.0, 5.0, 6.0, 7.0,
				          8.0, 9.0, 10.0, 11.0 };

#ifndef TEST_PGM
#ifndef TEST_DCT
int main(int argc, char *argv[]) {

	int32_t iRC;
	double *pInput = NULL;
	double *pResult = NULL;
	int32_t iInputLen;
	int32_t i=0;  // init to zero to read in debugger before i is used in loop
	int32_t j;
	int32_t k;  // for work with the thresholding
	int32_t m;  // m = num rows
	int32_t n;  // n = num cols
	int32_t offset;  // used to convert from (i,j) to linear index
	double value; // used to threshold the dct results
	double pNeighborhood[9];
	double pDctNeigh[9];
	int numValsAboveThreshold;

	// First pass, do copies instead of using pointers
	double NW, N, NE, W, pixel, E, SW, S, SE;

	iRC = open_pgm((char *) pFileName1);
	if (1 != iRC) {
		// he uses windows style return codes instead of unix style
		return EXIT_FAILURE;
	}


	// Convert frame buffer to double in range 0..1





	//
	// Test simple input
	//
	iInputLen = frame_width * frame_height;

	pInput = (double *) malloc(sizeof(double) * iInputLen);
	if (NULL == pInput) {
		return EXIT_FAILURE;
	}

	
	// copy char input in range 0-255 to double in range 0..1
	for (i = 0; i < iInputLen; i++) {
		pInput[i] = (double) frame_buffer[i];
		pInput[i] = pInput[i] / 255.0;
	}



	m = frame_height; // 366 for small
	n = frame_width;  // 412 for small

	// Used to test neighborhood offsets
	//m = 3;
	//n = 4;
	//pInput = pOffsetTesty;

	for (j = 0; j < n; j++) { // col iteration
		for (i = 0; i < m; i++) { // row iteration

			// for cases where a neighbor does not exist, simply set to zero
			// ie. if first pixel, it has no neighbor to top or left.
			//
			// pixet offset will be 
			// offset = (i)*n + (j);  
			// adjust accoringly to get neighbor

			// Get value for NW pixel, if it does not exist set to zero.
			if (i > 0 && j > 0) {
				// NW = pInput(i - 1, j - 1);  
				offset = (i-1)*n + (j-1);  
				NW = pInput[offset];  
			}  else {
				NW = 0;
			}

			//	// Get value for N pixel, if it does not exist set to zero.
			if (i > 0 ) {
				// N = pInput(i - 1, j );  
				offset = ( i - 1 )*n + ( j  );
				N = pInput[offset];
			} else {
				N = 0;
			}


			// Get value for NE pixel, if it does not exist set to zero.
			if (i > 0 && (j < (n - 1)) ) {
				// NE = pInput(i - 1, j + 1 );  
				offset = ( i - 1 )*n + ( j + 1 );
				NE = pInput[offset];
			} else {
				NE = 0;
			}


			// Get value for W pixel, if it does not exist set to zero.
			if (j > 0) {
				// W = pInput(i, j - 1 );  
				offset = ( i )*n + ( j - 1 );
				W = pInput[offset];
			} else {
				W = 0;
			}



			// Get value for E pixel, if it does not exist set to zero.
			if ( j < (n-1) ) {
				// E = pInput(i, j + 1 );  
				offset = (i) *n + ( j + 1 );
				E = pInput[offset];
			} else {
				E = 0;
			}

			// Get value for SW pixel, if it does not exist set to zero.
			if ( (i < ( m - 1 ) ) && (j > 0) ) {
				// SW = pInput(i, j + 1 );  
				offset = (i+1) *n + ( j - 1 );
				SW = pInput[offset];
			} else {
				SW = 0;
			}

			// Get value for S pixel, if it does not exist set to zero.
			if (i < ( m - 1 ) ) {
				// S = pInput(i, j + 1 );  
				offset = ( i + 1 ) *n + ( j );
				S = pInput[offset];
			} else {
				S = 0;
			}

			// Get value for SE pixel, if it does not exist set to zero.
			if ((i < ( m - 1 )) && (j < ( n - 1)) ) {
				// SE = pInput(i + 1, j + 1 );  
				offset = ( i + 1 ) *n + ( j + 1 );
				SE = pInput[offset];
			} else {
				SE = 0;
			}


			// And the pixel of interest
			offset = ( i ) *n + ( j );
			pixel = pInput[offset];


			// Now we can build the neighborhood of pixels
			// first pass do this with init list
			// second pass do assignments
			// third pass do this as pointers and have dct work on pointers so no copies are done
			pNeighborhood[0] = NW;
			pNeighborhood[1] = N;
			pNeighborhood[2] = NE;
			pNeighborhood[3] = W;
			pNeighborhood[4] = pixel;
			pNeighborhood[5] = E;
			pNeighborhood[6] = SW;
			pNeighborhood[7] = S;
			pNeighborhood[8] = SE;


			iRC = dct0(pNeighborhood, 9, pDctNeigh);
			if (RC_ERROR == iRC) {
				return EXIT_FAILURE;
			}

			// dump neighborhood
//			dump_double_buffer(pDctNeigh, 9);

			// Threshold the values and
			// Determine the number of values after thresholding
			numValsAboveThreshold = 0;
			for (k = 0; k < 9; k++) {
				value = pDctNeigh[k];
				value = fabs(value);
				if (value > 0.08) {
					numValsAboveThreshold++;
				}
			}


			// Adjust highlight of pixel of interest
			if (numValsAboveThreshold < 3) { // highlights in focus
				pInput[offset] = 1;  // offset was calculated last for the pixel of interest. It has not changed.
			}



		}
		// Get an idea how fast this is working
		//	printf("finished column %d of %d.\n",j,n);
	}


	// Rewrite the frame buffer
	// copy double in range 0-1 to char in range 0..255
	for (i = 0; i < iInputLen; i++) {
		frame_buffer[i] = (char) ( pInput[i] * 255.0 );  
	}


	// Write the frame buffer to a file
	iRC = write_buffer_to_pgm_file(pFileName3, frame_buffer, frame_width, frame_height);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}





	// free the buffers
	free(frame_buffer);
	free(pInput);
	free(pResult);



	// Test for leaks
	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;

}
#endif
#endif

