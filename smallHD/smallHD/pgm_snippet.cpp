#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "pgm_snippet.h"
#include "smallHD.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	


unsigned char *frame_buffer = NULL;
unsigned int frame_width;
unsigned int frame_height;

#ifdef TEST_PGM
int main(int argc, char *argv[]) {

	int iRC;

	if (open_pgm("kitten.pgm")) {
		// do stuff
	}


	iRC = write_buffer_to_pgm_file("output.pgm", frame_buffer, frame_width, frame_height);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}


	free(frame_buffer);

	// Test for leaks
	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;
}
#endif

int open_pgm(char *filename) {
	FILE *handle;
	handle = fopen(filename, "rb");
	char byte_read;
	int int_read = 0;
	int int_read_valid = 0;
	int header_pos = 0;

	int width = 0;
	int height = 0;
	int max_val = 0;

	int ret_val = 0;
	if (handle == NULL) return 0;

	if (fgetc(handle) != 'P' ||
		fgetc(handle) != '5') goto bail;


	while (header_pos<3) {
		byte_read = fgetc(handle);
		if (byte_read >= '0' && byte_read <= '9') {
			int_read *= 10;
			int_read += byte_read - '0';
			int_read_valid = 1;
		} else {
			if (int_read_valid) {
				if (header_pos == 0) width = int_read;
				if (header_pos == 1) height = int_read;
				if (header_pos == 2) max_val = int_read;
				header_pos++;
				int_read = 0;
				int_read_valid = 0;
			}

			if (byte_read == '#') {
				do {
					byte_read = fgetc(handle);
				} while (byte_read != '\n');
			}
		}
	}

	printf("width: %i height: %i\n", width, height);
	if (max_val != 255) {
		printf("Don't handle other than 8bpp\n");
		goto bail;
	}
	frame_buffer = (unsigned char *) malloc(width*height);
	if (frame_buffer == NULL) {
		printf("No memory\n");
		goto bail;
	}
	fread(frame_buffer, 1, width*height, handle);
	frame_width = width;
	frame_height = height;
	ret_val = 1;
bail:
	fclose(handle);
	return ret_val;
}

/* This snipper will write a pgm not wrapped up yet
dump = fopen(out_name,"wb");
if(dump) {
fprintf(dump, "P5\n%i %i\n 255\n", frame_width,frame_height);
fwrite(outputbuff,1,frame_width*frame_height,dump);
fclose(dump);
}
*/


// The original given snippet did a version 5
// I am doing a version 2 for easy use. Its 8 bit color so this should owkr.
int write_buffer_to_pgm_file(const char *pchFileName, const unsigned char *pBuffer, const unsigned int width, const unsigned height) {


	FILE *dest;

	//dest = fopen(pchFileName, "wb");
	fopen_s(&dest, pchFileName, "wb");  // b for windows api

	if (NULL == dest) {
		printf("fail to open file for write\n");
		return RC_ERROR;
	}

	fprintf(dest, "P2\n%u %u\n", width, height);
	fprintf(dest, "255\n");

	unsigned int len = width * height;
	for (unsigned int i=0; i < len; i++) {
		fprintf(dest, "%u ", pBuffer[i]);
		if (0 == (i+1) % width) {
			fprintf(dest, "\n");
		}
	}


	fprintf(dest,"\n");


	fclose(dest);

	return RC_OK;
}

