#include <stdio.h>
#include <stdint.h>
#include "pgm_snippet.h"
#include "smallHD.h"
#define _USE_MATH_DEFINES // for C  . for c++ iuse cmath.h
#include <math.h>
#include "dct.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	


// iput output test for book
const int32_t ciNumDct0Test = 4;
double pDct0Test[ciNumDct0Test] = { 1.0, 2.0, 0.0, 3.0 }; // dct test data
double pDct0TestResults[ciNumDct0Test] = { 1.5, 0.25, -1.0, 0.25 }; // dct test data dct result


// input output test for matlab / octave
const int32_t ciNumOnes = 9;
double pOnes[ciNumOnes] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }; // a neighborhood of all ones
double pOnesResult[ciNumOnes] = { 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }; // a neighborhood of all ones dct result


// another input output test for matlab / octave
const int32_t ciNumNeighbors1 = 9;
double pNeighbors1[ciNumNeighbors1] = { 000.0,         128.0/255.0,   000.0, 
                                        128.0/255.0,   255.0/255.0,   128.0/255.0, 
	                                    000.0,         128.0/255.0,   000.0 }; // a neighborhood 
double pNeighbors1Result[ciNumNeighbors1] = { 1.0026,    0.0,     -0.5973,
                                              0.0000,    0.31696,  0.000, 
	                                         -0.7080,    0.0,     -0.2099 }; // a neighborhood dct result


// yet another input output test for matlab / octave
const int32_t ciNumNeighbors2 = 9;
double pNeighbors2[ciNumNeighbors2] = { 128.0 / 255.0,   128.0 / 255.0,   128.0 / 255.0,
                                        128.0 / 255.0,   255.0 / 255.0,   128.0 / 255.0,
	                                    128.0 / 255.0,   128.0 / 255.0,   128.0 / 255.0 }; // a neighborhood 
double pNeighbors2Result[ciNumNeighbors2] = {  1.67190,    0.0,      -0.23478,
                                               0.0000,     0.23478,   0.000,
                                              -0.23478,     0.00000,   0.23478 }; // a neighborhood dct result




#ifdef TEST_DCT

int main(int argc, char *argv[]) {


	double *pInput = NULL;
	double *pResult = NULL;
	double *pExpectedResults = NULL;
	int32_t iRC;
	int32_t iResultsLen;
	int32_t i;
	double dDiff;

	//
	// Test simple input
	//
	iResultsLen = ciNumDct0Test;
	pInput = pDct0Test;
	pExpectedResults = pDct0TestResults;


	pResult = (double *) malloc( sizeof(double) * iResultsLen );
	if (NULL == pResult) {
		return EXIT_FAILURE;
	}

	// These results were from the book.  
	// The scaling is different from the matlab results
	// I wonder which one is faster?
	iRC = dct0(pInput, iResultsLen, pResult);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}

	dump_double_buffer(pResult, iResultsLen);

	// Verify results
	for (i = 0; i < iResultsLen; i++) {
		dDiff = fabs(pResult[i] - pExpectedResults[i]);
		if (dDiff > 0.1) {
			return EXIT_FAILURE;
		}
	}

	free(pResult);
	// End test simple input




	//
	// Test all ones input
	//
	iResultsLen = ciNumOnes;
	pInput = pOnes;
	pExpectedResults = pOnesResult;

	pResult = (double *) malloc(sizeof(double) * iResultsLen);
	if (NULL == pResult) {
		return EXIT_FAILURE;
	}

	// These results were from the octave help.    
	// I wonder which one is faster?
	iRC = dct(pInput, iResultsLen, pResult);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}

	dump_double_buffer(pResult, iResultsLen);

	// Verify results
	for (i = 0; i < iResultsLen; i++) {
		dDiff = fabs(pResult[i] - pExpectedResults[i]);
		if (dDiff > 0.1) {
			return EXIT_FAILURE;
		}
	}

	free(pResult);
	// End test all ones input





	//
	// Test neighborhood 1 input
	//
	iResultsLen = ciNumNeighbors1;
	pInput = pNeighbors1;
	pExpectedResults = pNeighbors1Result;

	pResult = (double *) malloc(sizeof(double) * iResultsLen);
	if (NULL == pResult) {
		return EXIT_FAILURE;
	}

	// These results were from the octave help.    
	// I wonder which one is faster?
	iRC = dct(pInput, iResultsLen, pResult);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}

	dump_double_buffer(pResult, iResultsLen);

	// Verify results
	for (i = 0; i < iResultsLen; i++) {
		dDiff = fabs(pResult[i] - pExpectedResults[i]);
		if (dDiff > 0.1) {
			return EXIT_FAILURE;
		}
	}

	free(pResult);
	// End test all ones input





	//
	// Test neighborhood 2 input
	//
	iResultsLen = ciNumNeighbors2;
	pInput = pNeighbors2;
	pExpectedResults = pNeighbors2Result;

	pResult = (double *) malloc(sizeof(double) * iResultsLen);
	if (NULL == pResult) {
		return EXIT_FAILURE;
	}

	// These results were from the octave help.    
	// I wonder which one is faster?
	iRC = dct(pInput, iResultsLen, pResult);
	if (RC_ERROR == iRC) {
		return EXIT_FAILURE;
	}

	dump_double_buffer(pResult, iResultsLen);

	// Verify results
	for (i = 0; i < iResultsLen; i++) {
		dDiff = fabs(pResult[i] - pExpectedResults[i]);
		if (dDiff > 0.1) {
			return EXIT_FAILURE;
		}
	}

	free(pResult);
	// End test all ones input





	// Test for leaks
	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;

}

#endif

// pBufferIn input buffer to compute DCT
// pResult output buffer to gather results
// length number of elements in each buffer
// assumes pBufferIn and pResult are allocated
// 
// This is the form from the book
int32_t dct0(const double *pBufferIn, const int32_t length, double *pResult) {

	int32_t n;
	int32_t k;
	int32_t N = length;
	double frequency;
	const double  twopi = M_PI * 2;  // from math with # define
	double Xc;
	double Xn;

	if (NULL == pBufferIn) {
		return RC_ERROR;
	}

	if (NULL == pResult) {
		return RC_ERROR;
	}



	// Using Digital Signal Processing book by Emmanuel C. Fleachor
	// from my bookshelf as a reference
	// page 88 whgere he is showing the math to do a DCT for three 
	// input values where he calculates the coefficients by hand.
	// It will be a good check since the values are explicitly listed
	// for each coefficient.  ie.
	// Xc(0) = 1/4 * x0 cos0 +x1 cost 0 ....
	//       = 1/4(1+2+0+3) 
	//		 = ...

	// Xc(k) = 1   N-1          +-  k 2 pi n
	//        ---  sum  xn  cos |  ----------    k=0,1,...,N-1
	//        N    n=0          +-    N
	for (k = 0; k < N; k++) {
		Xc = 0;
		for (n = 0; n < N; n++) {
			Xn = pBufferIn[n];
			frequency = ( k * twopi * n ) / N;
			Xc = Xc + Xn * cos(frequency);
		}
		Xc = Xc / N;
		pResult[k] = Xc;
	}



	return RC_OK;
}


// pBufferIn input buffer to compute DCT
// pResult output buffer to gather results
// length number of elements in each buffer
// assumes pBufferIn and pResult are allocated
// 
// This is the form from the matlab/octave
// When I went to check the results difference during implementation
// on windows, I downloaded octave.  In the octave help was this note:
//
//      The discrete cosine transform X can be defined as follows:
//
//					            N - 1
//					X[k] = w(k) sum x[n] cos(pi(2n + 1) k / 2N),  k = 0, ..., N - 1
//					            n = 0
//
//      with    w(0) = sqrt(1 / N) 
//      and     w(k) = sqrt(2 / N), k = 1, ..., N - 1.  
//
//      There are other definitions with different scaling of X[k], 
//      but this form is common in image processing.
//
//
int32_t dct(const double *pBufferIn, const int32_t length, double *pResult) {

	int32_t n;
	int32_t k;
	int32_t N = length;
	double frequency;
	const double  pi = M_PI;  // from math with # define
	double Xc;
	double Xn;
	double Wk;

	if (NULL == pBufferIn) {
		return RC_ERROR;
	}

	if (NULL == pResult) {
		return RC_ERROR;
	}


	// Change to matlab / octave syntax so I can check results
	//
	//					            N-1
	//					X[k] = w(k) sum x[n] cos(pi(2n + 1) k / 2N)         k = 0, ..., N - 1
	//					            n=0
	//
	//      with    w(0) = sqrt(1 / N) 
	//      and     w(k) = sqrt(2 / N), k = 1, ..., N - 1.  
	for (k = 0; k < N; k++) {
		Xc = 0;
		if (0 == k) {
			Wk = sqrt(1.0 / N);
		} else {
			Wk = sqrt(2.0 / N);
		}

		for (n = 0; n < N; n++) {
			Xn = pBufferIn[n];
			frequency = (  ( pi * (2.0 * n + 1.0) * k ) / (2.0 * N)  );
			Xc = Xc + Xn * cos(frequency);
		}
		Xc = Wk * Xc;
		pResult[k] = Xc;
	}



	return RC_OK;
}



int32_t dump_double_buffer(const double *pBufferIn, const int32_t length) {

	int32_t i;

	if (NULL == pBufferIn) {
		return RC_ERROR;
	}

	printf("\n------------------\n");
	for (i = 0; i < length; i++) {
		printf("%4.3f, ",pBufferIn[i]);
		if (0 == (i+1) % 3) {
			printf("\n");
		}
	}


	return RC_OK;

}


