#include <stdio.h>
#include <stdlib.h>


int open_pgm(char *filename) ;

unsigned char *frame_buffer=NULL;
unsigned int frame_width;
unsigned int frame_height;


int main(int argc, void **argv) {
	if(open_pgm("C:\\work\\SmallHD\\Genesis\\SmallHD-S2000\\research\\focus_assist\\kitten.pgm")) {
	// do stuff
	}
}

int open_pgm(char *filename) {
	FILE *handle;
	handle = fopen(filename, "rb");
	char byte_read;
	int int_read=0;
	int int_read_valid=0;
	int header_pos=0;

	int width = 0;
	int height = 0;
	int max_val = 0;

	int ret_val = 0;
	if(handle==NULL) return 0;

	if(fgetc(handle)!='P' ||
	   fgetc(handle)!='5') goto bail;


	while(header_pos<3) {
		byte_read = fgetc(handle);
		if(byte_read>='0' && byte_read<='9') {
			int_read *=10;
			int_read+=byte_read-'0';
			int_read_valid=1;
		} else {
			if(int_read_valid) {
				if(header_pos==0) width = int_read;
				if(header_pos==1) height= int_read;
				if(header_pos==2) max_val=int_read;
				header_pos++;
				int_read=0;
				int_read_valid=0;
			}

			if(byte_read=='#') {
				do {
					byte_read=fgetc(handle);
				} while(byte_read!='\n');
			}
		}
	}

	printf("width: %i height: %i\n",width, height);
	if(max_val!=255) {
		printf("Don't handle other than 8bpp\n");
		goto bail;
	}
	frame_buffer=malloc(width*height);
	if(frame_buffer==NULL) {
		printf("No memory\n");
		goto bail;
	}
	fread(frame_buffer,1,width*height,handle);
	frame_width = width;
	frame_height=height;
	ret_val=1;
bail:
	fclose(handle);
	return ret_val;
}

/* This snipper will write a pgm not wrapped up yet
		dump = fopen(out_name,"wb");
		if(dump) {
			fprintf(dump, "P5\n%i %i\n 255\n", frame_width,frame_height);
			fwrite(outputbuff,1,frame_width*frame_height,dump);
			fclose(dump);
		}
*/