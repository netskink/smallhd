# Notes

I did not get this job.  I should have noticed it did not make white the "in-focus" pixels.  However, they were asking me to do something I was unfamiliar with.  My trivial mistake was that as I iterated the pixel array, I would lighten a pixel and then later revisit a "lightened" array.


However, considering I wrote a prototype in Matlab, then another in purc C, I was not surprised I made a simple mistake like this.

Besides, it was a programming assignment for a job.

With that said, I am still proud of what I did.

The prototype is [here](prototype.pdf)
